// Trains an artificial neural network using supervised batch learning.
// Weights are modifyied to minimise the error between a set of target
// outputs and a set of calculated outputs from a set of inputs using
// conjugate gradient descent.
//
// (c) 2016: Marcel Admiraal

#ifndef BATCHTRAINNETWORK_H
#define BATCHTRAINNETWORK_H

#include "neuralnetwork.h"

#include "cgdescent.h"

namespace ma
{
    class BatchCostFunction : public GradientFunction
    {
    public:
        /**
         * Constructor.
         *
         * @param network       The network to train.
         * @param samples       The size of the target and input arrays.
         * @param target        The array of target outputs.
         * @param input         The array of inputs.
         * @param gamma         The regularisation factor.
         */
        BatchCostFunction(NeuralNetwork& network, const unsigned int samples,
                const Vector target[], const Vector input[],
                const double gamma = 10.0);

        /**
         * Populates the regularised cost and cost gradients for and with
         * respect to the specified weights.
         *
         * @param cost      The network's regularised cost.
         * @param gradient  The network's regularised cost gradients with
         * @param weight    The vector with the weights.
         *                  respect to the weights.
         */
        void getValueAndGradients(double& cost, Vector& costGradient,
                const Vector& weight);

        /**
         * Returns the current network.
         *
         * @return The current network.
         */
        NeuralNetwork& getNetwork() const;

        /**
         * Sets the network to be used.
         *
         * @param network   The new network.
         */
        void setNetwork(NeuralNetwork& network);

        /**
         * Returns the current regularisation factor.
         *
         * @param gamma    The new regularisation factor.
         */
        double getGamma() const;

        /**
         * Sets the regularisation factor.
         *
         * @param gamma    The new regularisation factor.
         */
        void setGamma(const double gamma);

    private:
        NeuralNetwork& network;
        unsigned int layers;
        unsigned int parameters;
        Vector layerSize;
        unsigned int samples;
        const Vector* target;
        const Vector* input;
        double gamma;
    };

    /**
     * Trains an artificial neural network using supervised batch learning.
     * Weights are modifyied to minimise the error between a set of target
     * outputs and a set of calculated outputs from a set of inputs using
     * conjugate gradient descent.
     *
     * @param network       The network to train.
     * @param samples       The size of the target and input arrays.
     * @param target        The array of target outputs.
     * @param input         The array of inputs.
     * @param gamma         The regularisation factor. Defaults to 10.
     * @param iterations    The maximum number of iterations. If 0 or not
     *                      specified uses the size of the network.
     */
    void batchTrain(NeuralNetwork& network, const unsigned int samples,
        const Vector target[], const Vector input[],
        const double gamma = 10, unsigned int iterations = 0);
}

#endif // BATCHTRAINNETWORK_H
