// The functions for modifying and calculating the costs and derivatives for
// the different neuron types that can be used in a neural network.
//
// (c) 2015 - 2017: Marcel Admiraal

#ifndef NEURON_H
#define NEURON_H

#include <iostream>

namespace ma
{
    enum class NeuronType
    {
        Normal,
        Binary,
        Threshold,
        Logistic,
        HyperbolicTangent,
        StochasticBinary
    };

    /**
     * Returns the neuron output modified by the Neuron Type.
     *
     * @param raw   The raw ouptut to be modified.
     * @param type  The neuron type used to modify the raw output.
     * @return      The output modified by the Neuron Type.
     */
    double modify(const double raw, const NeuronType type);

    /**
     * Returns the root mean square error modified by the Neuron Type.
     *
     * @param output    The modified output.
     * @param actual    The actual or correct output.
     * @param type      The neuron type used to modify the raw output.
     * @return          The root mean square error modified by the Neuron Type.
     */
    double cost(const double output, const double actual, const NeuronType type);

    /**
     * Returns the derivative of the output modified by the Neuron Type.
     *
     * @param output    The output of the neuron.
     * @param type      The neuron type used to modify the raw output.
     * @param raw       Whether or not the data is the raw data.
     * @return          The derivative of the output modified by the Neuron Type.
     */
    double derivative(const double output, const NeuronType type,
            const bool raw = false);

    /**
     * Streams the NeuronType in human readable form.
     *
     * @param os     The output stream to stream to.
     */
    void print(const NeuronType type, std::ostream& os = std::cout);
}

#endif // NEURON_H
