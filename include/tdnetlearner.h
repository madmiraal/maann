// A temporal difference artificial neural network real-time learning algorithm.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef TDNETLEARNER_H
#define TDNETLEARNER_H

#include "netlearner.h"

namespace ma
{
    class TDNetLearner : public NetLearner
    {
    public:
        /**
         * State length constructor.
         *
         * Creates a default network with an input size equal to the state
         * length, a single hidden layer with twice as many neurons as the input
         * layer, and a output size equal to the output length.
         *
         * @param stateLength   The input state vector length. Default: 2.
         * @param outputLength  The output vector length. Default: 1.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         * @param lambda        The prediction discount rate. Default: 0.7.
         */
        TDNetLearner(const unsigned int stateLength = 2,
                const unsigned int outputLength = 1,
                const double learnRate = 0.1, const double discountRate = 1.0,
                const double gamma = 0.001, const double lambda = 0.7);

        /**
         * Vector constructor.
         *
         * Creates a network with the layer sizes specified in the vector.
         *
         * @param layerSize     The layer sizes of the network.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         * @param lambda        The prediction discount rate. Default: 0.7.
         */
        TDNetLearner(const Vector& layerSize, const double learnRate = 0.1,
                const double discountRate = 1.0, const double gamma = 0.001,
                const double lambda = 0.7);

        /**
         * Network constructor.
         *
         * @param network       The network to train.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         * @param lambda        The prediction discount rate. Default: 0.7.
         */
        TDNetLearner(const NeuralNetwork network, const double learnRate = 0.1,
                const double discountRate = 1.0, const double gamma = 0.001,
                const double lambda = 0.7);

        /**
         * File constructor.
         *
         * @param filename  The filename to load the learner from.
         */
        TDNetLearner(const char* filename);

        /**
         * Copy constructor.
         *
         * @param source    The learner to copy.
         */
        TDNetLearner(const TDNetLearner& source);

        /**
         *  Default destructor
         */
         virtual ~TDNetLearner();

        /**
         * Swap function.
         *
         * @param first  The first learner to swap with.
         * @param second The second learner to swap with.
         */
        friend void swap(TDNetLearner& first, TDNetLearner& second);

        /**
         * Assignment operator.
         * Returns a copy of the learner.
         *
         * @param source The learner to copy.
         * @return       A copy of the learner.
         */
        TDNetLearner& operator=(TDNetLearner source);

        /**
         * Saves the current learner to a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the save was successful.
         */
        bool save(const char* filename) const;

        /**
         * Loads a previously saved learner from a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the load was successful.
         */
        bool load(const char* filename);

        /**
         * Inform the learner of the next state in a sequence, and any interim
         * reward received.
         * Updates the network's weights based on the difference between the
         * calculated output of this input and the previous input and the
         * incremental reward received.
         *
         * @param state     The next state in the sequence.
         * @param reward    The incremental reward received. Defaults to zero.
         */
        virtual void nextState(const Vector& state, const Vector& reward = Vector());

        /**
         * Inform the learner of the final reward received at the end of a
         * sequence.
         *
         * @param reward    The final reward received.
         */
        virtual void finalReward(const Vector& reward);

        /**
         * Inform the learner to discard the current sequence, because
         * squence terminated before a final reward was received.
         */
        virtual void sequenceReset();

        /**
         * Returns the current prediction discount rate.
         *
         * @return The current prediction discount rate.
         */
        double getLambda() const;

        /**
         * Sets the prediction weight discount.
         *
         * @param newRate   The new prediction discount rate.
         */
        void setLambda(const double newLambda);

    protected:
        // Update the network variables using the target.
        void updateVariables(const Vector& target);

        // The prediction discount rate.
        double lambda;
        // Records whether there have been previous states in the sequence.
        bool isPreviousState;
        // Records whether or not there has been a previous gradient.
        bool isPreviousGradient;
        // The previous state.
        Vector previousState;
        // The gradient sum.
        Vector predictionGradientSum;
    };

    /**
     * Input stream operator.
     * Builds a learner from the input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The learner to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, TDNetLearner& destination);

    /**
     * Output stream operator.
     * Appends the learner to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The learner to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const TDNetLearner& source);
}

#endif // TDNETLEARNER_H
