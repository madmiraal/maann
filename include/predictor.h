// A generic real-time learning predictor.
//
// Currently supports the MAUtils MCMCLearner, the MCNetLearner and the
// TDNetLearner.
//
// (c) 2016 - 2019: Marcel Admiraal

#ifndef PREDICTOR_H
#define PREDICTOR_H

#include "learner.h"
#include "vector.h"
#include "neuron.h"

#include <iostream>

namespace ma
{
    enum class LearnerType
    {
        MCMCLearner,
        MCNetLearner,
        TDNetLearner
    };

    class Predictor
    {
    public:
        /**
         * Default constructor.
         */
        Predictor();

        /**
         * Default destructor.
         */
        virtual ~Predictor();

        /**
         * Get predictor's current prediction.
         *
         * @param state The input vector representing the current state.
         * @return      The output vector representing the predictions.
         */
        Vector getPrediction(const Vector& state);

        /**
         * Inform predictor of the next state.
         *
         * @param state The next state.
         */
        void nextState(const Vector& state);

        /**
         * Inform predictor of the actual result.
         *
         * @param resultIndex   The index of the output that was correct.
         */
        void actualResult(const unsigned int resultIndex);

        /**
         * Inform predictor of the actual result.
         *
         * @param result    The actual result vector.
         */
        void actualResult(const Vector& result);

        /**
         * Reset sequence without final result.
         */
        void sequenceReset();

        /**
         * Create new default learner.
         *
         * @param learnerType   The type of learner to create.
         */
        void newLearner(const LearnerType learnerType);

        /**
         * Returns the type of learner.
         *
         * @param learnerType   The type of learner.
         */
        LearnerType getLearnerType() const;

        /**
         * Returns whether or not the learner is of the specified type.
         *
         * @param learnerType   The type of learner.
         */
        bool isLearnerType(const LearnerType learnerType) const;

        /**
         * Create new MCMC learner for predictor.
         *
         * @param stateLength       The state vector length.
         *                          Default: Use current.
         * @param predictionLength  The prediction vector length.
         *                          Default: Use current.
         * @param learnRate         The learning rate. Default: 0.1.
         * @param discountRate      The reward discount rate. Default: 1.0.
         * @param defaultValue      The default vector values. Default: 0.5.
         */
        void newMCMCLearner(unsigned int stateLength = 0,
                unsigned int predictionLength = 0,
                const double learnRate = 0.001,
                const double discountRate = 1.0,
                const double defaultValue = 0.5);

        /**
         * Create new MC network learner for predictor.
         *
         * Default: Keep current state and prediction vector lengths, and a
         * network with a single hidden layer with twice as many neurons as the
         * state legnth, and logistic neurons.
         *
         * @param layerSize     The vector containing the layer sizes.
         * @param hiddenType    The neuron type for the hidden layers.
         *                      Default: Logistic
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         */
        void newMCNetLearner(Vector layerSize = Vector(),
                const NeuronType hiddenType = NeuronType::Logistic,
                const double learnRate = 0.001, const double discountRate = 1.0,
                const double gamma = 0.001);

        /**
         * Create new TD network learner for predictor.
         *
         * Default: Keep current state and prediction vector lengths, and a
         * network with a single hidden layer with twice as many neurons as the
         * state legnth,and logistic neurons.
         *
         * @param layerSize     The vector containing the layer sizes.
         * @param hiddenType    The neuron type for the hidden layers.
         *                      Default: Logistic
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         * @param lambda        The prediction discount rate. Default: 0.7.
         */
        void newTDNetLearner(Vector layerSize = Vector(),
                const NeuronType hiddenType = NeuronType::Logistic,
                const double learnRate = 0.001, const double discountRate = 1.0,
                const double gamma = 0.001, const double lambda = 0.7);

        /**
         * Loads the learner from the file specified.
         * Note: Requires the current state and prediction lengths to match the
         * learner being loaded's input and output lengths.
         *
         * @param filename  The filename to load the learner from.
         * @return          Whether or not the learner was succesfully loaded.
         */
        bool loadLearner(const char* filename);

        /**
         * Saves the learner to the file specified. Returns true on success.
         *
         * @param filename  The filename to save the learner to.
         * @return          Whether or not the learner was succesfully saved.
         */
        bool saveLearner(const char* filename);

        /**
         * Sets whether the predictor is learning.
         *
         * @param learning  Whether or not the learner should learn.
         *                  Default: true.
         */
        void setLearning(bool learning = true);

        /**
         * Returns whether or not the predictor is currently learning.
         *
         * @return Whether or not the predictor is currently learning.
         */
        bool isLearning() const;

        /**
         * Returns the state vector length.
         *
         * @return The state vector length.
         */
        unsigned int getStateLength() const;

        /**
         * Returns the prediction vector length.
         *
         * @return The prediction vector length.
         */
        unsigned int getPredictionLength() const;

        /**
         * Adjusts the state and prediction vector lengths.
         *
         * @param stateLength       The new state vector length.
         * @param predictionLength  The new output vector length.
         */
        void adjustPredictor(const unsigned int stateLength,
                const unsigned int predictionLength);

        /**
         * Returns the current learning rate.
         *
         * @return The current learning rate.
         */
        double getLearningRate() const;

        /**
         * Sets the learning rate.
         *
         * @param learingRate   The new learning rate.
         */
        void setLearningRate(const double learningRate);

        /**
         * Returns the current discount rate.
         *
         * @return The current discount rate.
         */
        double getDiscountRate() const;

        /**
         * Sets the discount rate.
         *
         * @param discountRate  The new discount rate.
         */
        void setDiscountRate(const double discountRate);

        /**
         * Returns a vector containing the network's layers' sizes.
         *
         * @return  A vector with the number of neurons in each layer.
         */
        Vector getLayerSizes() const;

        /**
         * Returns the regularisation factor if using a net learner.
         *
         * @return The regularisation factor.
         */
        double getRegularisationFactor() const;

        /**
         * Set regularisation factor if using a net learner.
         *
         * @param gamma The new regularisation factor.
         */
        void setRegularisationFactor(const double gamma);

        /**
         * Returns the prediction discount rate if using a TD Net learner.
         *
         * @return The prediction discount rate.
         */
        double getLambda() const;

        /**
         * Sets the prediction discount rate if using a TD Net learner.
         *
         * @param newLambda   The new prediction discount rate.
         */
        void setLambda(const double newLambda);

    private:
        bool loadMCMCLearner(std::istream& is);
        bool loadMCNetLearner(std::istream& is);
        bool loadTDNetLearner(std::istream& is);

        Learner* learner;
        LearnerType learnerType;
        bool learning;
    };
}

#endif // PREDICTOR_H
