// A generic artificial neural network that supports any number of hidden
// layers, layers of any size and multiple output functions.
//
// (c) 2015 - 2017: Marcel Admiraal

#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#include "neuron.h"
#include "vector.h"

#include <iostream>

namespace ma
{
    class NeuralNetwork
    {
    public:
        /**
         * Constructor.
         *
         * @param layerSize A vector with the number of neurons in each layer.
         * @param type      The neurons' type. Default: Logistic.
         */
        NeuralNetwork(const Vector& layerSize = Vector(),
                const NeuronType type = NeuronType::Logistic);

        /**
         * Default destructor.
         */
        virtual ~NeuralNetwork();

        /**
         * Copy constructor.
         *
         * @param source    The neural network to copy.
         */
        NeuralNetwork(const NeuralNetwork& source);

        /**
         * Constructs a neural network from the specified file.
         *
         * @param filename  The name of the file that contains the network
         *                  specification.
         */
        NeuralNetwork(const char* filename);

        /**
         * Swap function.
         *
         * @param first     The first neural network to swap with.
         * @param second    The second neural network to swap with.
         */
        friend void swap(NeuralNetwork& first, NeuralNetwork& second);

        /**
         * Assignment operator.
         * Returns a copy of the neural network.
         *
         * @param source    The neural network to copy.
         * @return          A copy of the neural network.
         */
        NeuralNetwork& operator=(NeuralNetwork source);

        /**
         * Creates a new network, deleting the existing one.
         *
         * @param layerSize A vector with the number of neurons in each layer.
         * @param type      The neurons' type. Default: Logistic.
         */
        void create(const Vector& layerSize,
                const NeuronType type = NeuronType::Logistic);

        /**
         * Returns the number of parameters in the network.
         *
         * @return The number of parameters in the network.
         */
        unsigned int getParameters() const;

        /**
         * Returns the number of layers in the network.
         *
         * @return  The number of layers in the network.
         */
        unsigned int getLayers() const;

        /**
         * Returns a vector containing the network's layers' sizes.
         *
         * @return  A vector with the number of neurons in each layer.
         */
        Vector getLayerSizes() const;

        /**
         * Adjusts the sizes of the layers in the network.
         * If the number of layers changes a new network is created.
         * Existing weights are kept unchanged where possible.
         *
         * @param layerSize Vector with the new number of neurons in each layer.
         */
        void setLayerSizes(const Vector& layerSize);

        /**
         * Returns the number of neurons in the layer specified.
         *
         * @param layerIndex    The layer required.
         * @return              The number of neurons in the layer specified.
         */
        unsigned int getLayerSize(const unsigned int layerIndex) const;

        /**
         * Adjusts the size of a layer in the network.
         * If the layer index is greater than the size of the network,
         * a new network is created.
         * Existing weights are kept unchanged where possible.
         *
         * @param layerIndex    The layer to adjust.
         * @param layerSize     The new number of neurons in the layer.
         */
        void setLayerSize(const unsigned int layerIndex,
                const unsigned int layerSize);

        /**
         * Returns all the neural network's current weights as a vector.
         *
         * @return All the neural network's current weights as a vector.
         */
        Vector getWeights() const;

        /**
         * Returns all the neural network's current weights as a vector with the
         * bias weights set to zero.
         *
         * @return All the neural network's current weights as a vector with the
         * bias weights set to zero.
         */
        Vector getNonBiasWeights() const;

        /**
         * Returns the specified neuron link's current weight.
         *
         * @param layerIndex    The layer containing the neuron required.
         * @param neuronIndex   The neuron required.
         * @param inputIndex    The neuron's input required.
         * @return  The specified link's current weight.
         */
        double getWeight(const unsigned int layerIndex,
                const unsigned int neuronIndex,
                const unsigned int inputIndex) const;

        /**
         * Sets all the neural network's current weights to the values in the
         * vector.
         * Returns false if the vector is not the same size as the number of
         * weights in the neural network.
         *
         * @param weight    The vector containing the new weights.
         * @return          Whether or not the assignment was successful.
         */
        bool setWeights(const Vector& weightVector);

        /**
         * Sets the specified neuron link's current weight.
         *
         * @param layerIndex    The layer containing the neuron required.
         * @param neuronIndex   The neuron required.
         * @param inputIndex    The neuron's input required.
         * @param value         The value to set the weight to.
         * @return              Whether or not the assignment was successful.
         */
        bool setWeight(const unsigned int layerIndex,
                const unsigned int neuronIndex,
                const unsigned int inputIndex, const double value);

        /**
         * Resets the weights to random initial values which depend on the
         * network size.
         */
        void initialiseWeights();

        /**
         * Returns all the neural network's current bias settings as a vector:
         * one for on and zero for off.
         *
         * @return All the neural network's current biases as a vector.
         */
        Vector getBiases() const;

        /**
         * Sets each layer's bias to on if the corresponding value is not equal
         * to zero.
         * Returns false if the vector is not the same size as the number of
         * biases in the neural network i.e. layers - 1.
         *
         * @param bias  The vector containing the new bias settings.
         * @return      Whether or not the assignment was successful.
         */
        bool setBiases(const Vector& bias);

        /**
         * Sets all the biases to 1.0 or 0.0.
         *
         * @param use   Whether to use biases i.e. set them to 1.0 or not i.e.
         *              set them to 0.0.
         */
        void setBiases(const bool use = true);

        /**
         * Sets the specified bias to 1.0 or 0.0.
         *
         * @param layerIndex    The layer required.
         * @param use           Whether the specified layer uses a bias i.e.
         *                      set it to 1.0 or not i.e. set it to 0.0.
         * @return              Whether or not the assignment was successful.
         */
        bool setBias(const unsigned int layerIndex, const bool use = true);

        /**
         * Returns the required neuron's type.
         *
         * @param layerIndex    The layer required.
         * @param neuronIndex   The neuron required.
         * @return              The required neuron's type.
         */
        NeuronType getNeuronType(const unsigned int layerIndex,
                const unsigned int neuronIndex) const;

        /**
         * Sets the specified neuron's type.
         *
         * @param layerIndex    The layer required.
         * @param neuronIndex   The neuron required.
         * @param newNeuronType The neuron's new type.
         * @return              Whether or not the assignment was successful.
         */
        bool setNeuronType(const unsigned int layerIndex,
                const unsigned int neuronIndex, NeuronType newNeuronType) const;

        /**
         * Sets the specified layer's neurons' types.
         *
         * @param layerIndex    The layer required.
         * @param newNeuronType The neuron's new type.
         * @return              Whether or not the assignment was successful.
         */
        bool setNeuronType(const unsigned int layerIndex,
                NeuronType newNeuronType) const;

        /**
         * Sets all the network's neurons' types.
         *
         * @param newNeuronType The neuron's new type.
         */
        void setNeuronType(NeuronType newNeuronType) const;

        /**
         * Returns all the neural network's current neuron inputs as a vector,
         * which includes the biases.
         *
         * @return All the neural network's current neuron inputs as a vector.
         */
        Vector getNeuronInputs() const;

        /**
         * Returns the specified layer's neuron inputs i.e. the outputs of the
         * layer below the layer specified, which includes the bias.
         *
         * @param layerIndex    The layer whose inputs are required.
         * @return              The layer's neuron inputs.
         */
        Vector getNeuronInputs(const unsigned int layerIndex) const;

        /**
         * Returns all the neural network's current neuron outputs as a vector,
         * which excludes the biases.
         *
         * @return All the neural network's current neuron outputs as a vector.
         */
        Vector getNeuronOutputs() const;

        /**
         * Returns the specified layer's neuron outputs i.e. excluding the bias.
         *
         * @param layerIndex    The layer whose outputs are required.
         * @return              The layer's neuron outputs.
         */
        Vector getNeuronOutputs(const unsigned int layerIndex) const;

        /**
         * Returns the network output for the provided input.
         * Uses previous input and calculated output if input is not provided.
         * Returns a zero vector if the input vector is the incorrect length.
         *
         * @param input     The input vector. Defaults to previous input.
         * @return          The calculated output vector, or
         *                  the zero vector if the input vector is the incorrect
         *                  length.
         */
        Vector getOutput(const Vector& input = Vector());

        /**
         * Calculates and returns the cost between the target output and the
         * calculated output from the input.
         * Uses previous input and calculated output if input is not provided.
         * Returns zero if either target or input vectors are the incorrect
         * length.
         *
         * @param target    The target output vector.
         * @param input     The input vector. Defaults to previous input.
         * @param gamma     The regularisation factor. Default: 0.
         * @return          The cost.
         */
        double getCost(const Vector& target, const Vector& input = Vector(),
                const double gamma = 0);

        /**
         * Returns the specified layer's errors.
         * Calculates, back propagates the errors given the target output and
         * the calculated output from the input.
         * Uses previously calclulated errors if target is not provided.
         * Uses previous input and calculated output if input is not provided.
         * Returns a zero vector if either target or input vectors are the
         * incorrect length.
         *
         * @param layerIndex    The required layer.
         * @param target        The target output vector.
         * @param input         The input vector.
         * @return              The layer's errors as a vector.
         */
        Vector getErrors(const unsigned int layerIndex,
                const Vector& target = Vector(), const Vector& input = Vector());

        /**
         * Returns all the network's errors.
         * Calculates, back propagates the errors given the target output and
         * the calculated output from the input.
         * Uses previously calclulated errors if target is not provided.
         * Uses previous input and calculated output if input is not provided.
         * Returns a zero vector if either target or input vectors are the
         * incorrect length.
         *
         * @param target    The target output vector.
         * @param input     The input vector.
         * @return          All the errors as a vector.
         */
        Vector getErrors(const Vector& target = Vector(),
                const Vector& input = Vector());

        /**
         * Calculates and returns all the cost gradients with respect to the
         * weights.
         * Uses previously calclulated errors if target is not provided.
         * Uses previous input and calculated output if input is not provided.
         *
         * @param target    The target output vector.
         * @param input     The input vector.
         * @param gamma     The regularisation factor. Defaults to 0.
         * @return          All the cost gradients as a vector.
         */
        Vector getCostGradients(const Vector& target = Vector(),
                const Vector& input = Vector(), const double gamma = 0);

        /**
         * Streams the contents of the neural network laid out in human
         * readable form.
         *
         * @param os     The output stream to stream to.
         */
        void print(std::ostream& os = std::cout) const;

        /**
         * Saves the neural network to the specified file.
         *
         * @param filename  The name of the file to save the neural network to.
         * @return          True if successful.
         */
        bool save(const char* filename) const;

        /**
         * Replaces the current neural network with one saved in the file.
         *
         * @param filename  The name of the file to load the neural network from.
         * @return          True if successful.
         */
        bool load(const char* filename);

    private:
        // Build a network with layer sizes and neuron type specified.
        void build(const Vector& layerSize = Vector(),
                NeuronType type = NeuronType::Logistic);
        // Relesae the memory used by the network.
        void destroy();
        // Set the network input.
        bool setInput(const Vector& input);
        // Calculate the output from the input layer by layer.
        bool calculateOutput(const Vector& input);
        // Backpropagate the errors given the target output.
        bool backPropagateErrors(const Vector& target);

        // A vector containing the sizes of the layers.
        Vector layerSize;
        // Stores the number of layers.
        unsigned int layers;
        // Stores the number of weights.
        unsigned int weights;
        // The network weights.
        double*** weight;
        // The network's neurons' values.
        double** neuronValue;
        // The network's output neurons' errors.
        double** error;
        // The network's output neurons' types.
        NeuronType** neuronType;
    };

    /**
     * Returns whether or not two neural networks' layers are all the same size,
     * all the weights are approximately equal, and
     * all the neuron types are the same.
     *
     * @param left  The first neural network.
     * @param right The second neural network.
     * @param error The percentage error allowed. Defaults to 0.01%
     * @return      True if the neural networks' layers are all the same size,
     *              all the weights are approximately equal, and
     *              the neruon types are the same.
     */
    bool approxEqual(const NeuralNetwork& left, const NeuralNetwork& right,
            const double error = 0.01);

    /**
     * Input stream operator.
     * Builds a neural network from the input stream and
     * returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The neural network to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, NeuralNetwork& destination);

    /**
     * Output stream operator.
     * Appends the neural network to the output stream and
     * returns the output stream.
     *
     * @param os     The output stream.
     * @param source The neural network to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const NeuralNetwork& source);

    /**
     * Outputs the neural network in human readable form.
     *
     * @param source The neural network to print.
     * @param os     The output stream to print to.
     */
    void print(const NeuralNetwork& source, std::ostream& os = std::cout);
}
#endif // NEURALNETWORK_H
