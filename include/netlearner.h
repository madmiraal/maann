// Abstract class for a artificial neural network temporal learning algorithm.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef NETLEARNER_H
#define NETLEARNER_H

#include "learner.h"
#include "neuralnetwork.h"

#include <tuple>

namespace ma
{
    class NetLearner : public Learner
    {
    public:
        /**
         * State length constructor.
         *
         * Creates a default network with an input size equal to the state
         * length, a single hidden layer with twice as many neurons as the input
         * layer, an output size equal to the output length, and logistic
         * neurons.
         *
         * @param stateLength   The input state vector length. Default: 2.
         * @param outputLength  The output vector length. Default: 1.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         */
        NetLearner(const unsigned int stateLength = 2,
                const unsigned int outputLength = 1,
                const double learnRate = 0.1,
                const double discountRate = 1.0,
                const double gamma = 0.001);

        /**
         * Vector constructor.
         *
         * Creates a network with the layer sizes specified in the vector.
         *
         * @param layerSize     The layer sizes of the network.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         * @param type          The neurons' type. Default: Logistic.
         */
        NetLearner(const Vector& layerSize, const double learnRate = 0.1,
                const double discountRate = 1.0, const double gamma = 0.001,
                const NeuronType type = NeuronType::Logistic);

        /**
         * Network constructor.
         *
         * @param network       The network to train.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         */
        NetLearner(const NeuralNetwork& network, const double learnRate = 0.1,
                const double discountRate = 1.0, const double gamma = 0.001);

        /**
         * Copy constructor.
         *
         * @param source    The learner to copy.
         */
        NetLearner(const NetLearner& source);

        /**
         * Default destructor.
         */
        virtual ~NetLearner();

        /**
         * Swap function.
         *
         * @param first  The first learner to swap with.
         * @param second The second learner to swap with.
         */
        friend void swap(NetLearner& first, NetLearner& second);

        /**
         * Saves the current learner to a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the save was successful.
         */
        virtual bool save(const char* filename) const = 0;

        /**
         * Loads a previously saved learner from a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the load was successful.
         */
        virtual bool load(const char* filename) = 0;

        /**
         * Returns the current vector value of the state.
         *
         * @param state A vector representation of the state.
         * @return      The learner's current vector value of the state.
         */
        virtual Vector getStateValue(const Vector& state);

        /**
         * Inform the learner of the next state in a sequence, and any interim
         * reward received.
         *
         * @param state     The next state in the sequence.
         * @param reward    Interim reward. Defaults to zero.
         */
        virtual void nextState(const Vector& state,
                const Vector& reward = Vector()) = 0;

        /**
         * Inform the learner of the final reward received at the end of a
         * sequence.
         *
         * @param reward    The final reward received.
         */
        virtual void finalReward(const Vector& reward) = 0;

        /**
         * Inform the learner to discard the current sequence, because
         * squence terminated before a final reward was received.
         */
        virtual void sequenceReset() = 0;

        /**
         * Adjusts the input and output state vector lengths.
         *
         * @param stateLength   The new state vector length.
         * @param outputLength  The new output vector length.
         */
        virtual void adjustLearner(const unsigned int stateLength,
                const unsigned int outputLength);

        /**
         * Returns a copy of the current network.
         *
         * @return  A copy of the current network.
         */
        NeuralNetwork getNetwork() const;

        /**
         * Sets the network.
         *
         * @param nework    The new network.
         */
        void setNetwork(const NeuralNetwork network);

        /**
         * Returns a vector containing the network's layers' sizes.
         *
         * @return  A vector with the number of neurons in each layer.
         */
        Vector getLayerSizes() const;

        /**
         * Create a new network.
         *
         * @param layerSize Vector with the number of neurons in each layer.
         */
        void createNetwork(const Vector& layerSize);

        /**
         * Adjust network layer sizes.
         *
         * @param layerSize Vector with the new number of neurons in each layer.
         */
        void adjustNetwork(const Vector& layerSize);

        /**
         * Set hidden layers neuron type.
         *
         * @param neuronType    The new hidden layer neruron type.
         */
        bool setHiddenNeuronType(const NeuronType hiddenNeuronType);

        /**
         * Returns the regularisation factor.
         *
         * @return The regularisation factor.
         */
        double getRegularisationFactor() const;

        /**
         * Set regularisation factor.
         *
         * @param gamma The new regularisation factor.
         */
        void setRegularisationFactor(const double gamma);

    protected:
        // The neural network.
        NeuralNetwork network;
        // The regularisation factor.
        double gamma;
    };
}

#endif // NETLEARNER_H
