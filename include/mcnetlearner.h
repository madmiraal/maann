// A Markov Chain artificial neural network learning algorithm.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef MCNETLEARNER_H
#define MCNETLEARNER_H

#include "netlearner.h"
#include "list.h"

#include <tuple>

namespace ma
{
    class MCNetLearner : public NetLearner
    {
    public:
        /**
         * State length constructor.
         *
         * Creates a default network with an input size equal to the state
         * length, a single hidden layer with twice as many neurons as the input
         * layer, and a output size equal to the output length.
         *
         * @param stateLength   The input state vector length. Default: 2.
         * @param outputLength  The output vector length. Default: 1.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         */
        MCNetLearner(const unsigned int stateLength = 2,
                const unsigned int outputLength = 1,
                const double learnRate = 0.1,
                const double discountRate = 1.0,
                const double gamma = 0.001);

        /**
         * Vector constructor.
         *
         * Creates a network with the layer sizes specified in the vector.
         *
         * @param layerSize     The layer sizes of the network.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         */
        MCNetLearner(const Vector& layerSize, const double learnRate = 0.1,
                const double discountRate = 1.0,
                const double gamma = 0.001);

        /**
         * Network constructor.
         *
         * @param network       The network to train.
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         */
        MCNetLearner(const NeuralNetwork& network, const double learnRate = 0.1,
                const double discountRate = 1.0,
                const double gamma = 0.001);

        /**
         * Copy constructor.
         *
         * @param source    The learner to copy.
         */
        MCNetLearner(const MCNetLearner& source);

        /**
         * File constructor.
         *
         * @param filename  The filename to load the learner from.
         */
        MCNetLearner(const char* filename);

        /**
         * Destructor.
         */
        virtual ~MCNetLearner();

        /**
         * Swap function.
         *
         * @param first  The first learner to swap with.
         * @param second The second learner to swap with.
         */
        friend void swap(MCNetLearner& first, MCNetLearner& second);

        /**
         * Assignment operator.
         * Returns a copy of the learner.
         *
         * @param source The learner to copy.
         * @return       A copy of the learner.
         */
        MCNetLearner& operator=(MCNetLearner source);

        /**
         * Saves the current learner to a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the save was successful.
         */
        bool save(const char* filename) const;

        /**
         * Loads a previously saved learner from a file with the given filename.
         *
         * @param filename  The filename to save the learner to.
         * @return          Return true if the load was successful.
         */
        bool load(const char* filename);

        /**
         * Inform the learner of the next state in a sequence, and any interim
         * reward received.
         *
         * @param state     The next state in the sequence.
         * @param reward    Interim reward. Defaults to zero.
         */
        void nextState(const Vector& state, const Vector& reward = Vector());

        /**
         * Inform the learner of the final reward received at the end of a
         * sequence.
         *
         * @param reward    The final reward received.
         */
        void finalReward(const Vector& reward);

        /**
         * Inform the learner to discard the current sequence, because
         * squence terminated before a final reward was received.
         */
        void sequenceReset();

    private:
        void updateRewards(const Vector& reward);

        // The states and rewards encountered in the current sequence.
        List<std::tuple<Vector, Vector> > sequence;
    };

    /**
     * Input stream operator.
     * Builds a learner from the input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The learner to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, MCNetLearner& destination);

    /**
     * Output stream operator.
     * Appends the learner to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The learner to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const MCNetLearner& source);
}

#endif // MCNETLEARNER_H
