# MAANN #

Cross-platform C++ code for a creating generic artificial neural networks.

* ma::NeuralNetwork: A generic neural network class for creating a neural network of as many layers and layer sizes as required.
* ma::Predictor: Uses MAUtils ma::Learner derived learners for making predictions while continually learning from new temporal data. Currently supports:
  * ma::MCMCLearner
  * ma::NetLearner
  * ma::TDNetLearner.

## Network Training ##
* ma::batchTrain: Trains an artificial neural network using supervised batch learning. Weights are modifyied to minimise the error between a set of target outputs and a set of calculated outputs from a set of inputs using conjugate gradient descent.
* ma::NetLearner: Extends the MAUtils ma::Learner to use temporal sequence of inputs with a single output to mini-batch train a neural network incrementally.
* ma::TDNetLearner: Extends the MAUtils ma::Learner to use temporal difference learing to train a neural network in real-time.

## Dependencies ##
This code depends on:

* [MAUtils](https://bitbucket.org/madmiraal/mautils)
