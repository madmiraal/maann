// (c) 2016: Marcel Admiraal

#include "batchtrainnetwork.h"
#include "neuralnetwork.h"

#include <ctime>

namespace ma
{
    BatchCostFunction::BatchCostFunction(NeuralNetwork& network,
            const unsigned int samples, const Vector target[],
            const Vector input[], const double gamma) :
            network(network), samples(samples), target(target), input(input),
            gamma(gamma)
    {
        layers = network.getLayers();
        parameters = network.getParameters();
        layerSize = network.getLayerSizes();
    }

    void BatchCostFunction::getValueAndGradients(double& cost,
            Vector& costGradient, const Vector& weight)
    {
        // Apply the weights to the network.
        if (!network.setWeights(weight)) throw "Weights not set.";

        // Get the total cost and gradients for all the samples.
        cost = 0.0;
        costGradient = Vector(parameters);
        for (unsigned int sample = 0; sample < samples; ++sample)
        {
            cost += network.getCost(target[sample], input[sample], 0);
            Vector thisGradient = network.getCostGradients(target[sample]);
            costGradient += thisGradient;
        }

        // Get the non-bias weights, because we do not regularise the bias.
        Vector nonBiasWeight = network.getNonBiasWeights();

        // Regularise cost.
        cost += gamma / 2 * nonBiasWeight.elementSquaredSum();
        cost /= samples;
        // Note we do not divide the cost by 2 because,
        // we're using the modified cost function.

        // Regularise the gradients.
        costGradient += gamma * nonBiasWeight;
        costGradient /= samples;
    }

    NeuralNetwork& BatchCostFunction::getNetwork() const
    {
        return network;
    }

    void BatchCostFunction::setNetwork(NeuralNetwork& network)
    {
        this->network = network;
        layers = network.getLayers();
        parameters = network.getParameters();
        layerSize = network.getLayerSizes();
    }

    double BatchCostFunction::getGamma() const
    {
        return gamma;
    }

    void BatchCostFunction::setGamma(const double gamma)
    {
        this->gamma = gamma;
    }

    void batchTrain(NeuralNetwork& network, const unsigned int samples,
            const Vector target[], const Vector input[],
            const double gamma, unsigned int iterations)
    {
        BatchCostFunction f(network, samples, target, input, gamma);
        if (iterations == 0) iterations = network.getParameters();
        Vector initialWeight = network.getWeights();
        Vector finalWeight = minimise(&f, initialWeight, iterations);
        network.setWeights(finalWeight);
    }
}
