// (c) 2016 - 2019: Marcel Admiraal

#include "predictor.h"

#include "mcmclearner.h"
#include "mcnetlearner.h"
#include "tdnetlearner.h"
#include "str.h"

#include <fstream>

namespace ma
{
    Predictor::Predictor() :
        learner(new MCMCLearner()), learnerType(LearnerType::MCMCLearner),
        learning(true)
    {
    }

    Predictor::~Predictor()
    {
        delete learner;
    }

    Vector Predictor::getPrediction(const Vector& state)
    {
        return learner->getStateValue(state);
    }

    void Predictor::nextState(const Vector& state)
    {
        if (learning)
        {
            learner->nextState(state);
        }
    }

    void Predictor::actualResult(const unsigned int resultIndex)
    {
        if (learning)
        {
            unsigned int predictionLength = learner->getOutputLength();
            Vector result(predictionLength);
            if (predictionLength == 1)
            {
                result[0] = resultIndex;
            }
            else
            {
                if (resultIndex >= predictionLength)
                {
                    learner->sequenceReset();
                    return;
                }
                result[resultIndex] = 1;
            }
            learner->finalReward(result);
        }
    }

    void Predictor::actualResult(const Vector& result)
    {
        if (learning)
        {
            learner->finalReward(result);
        }
    }

    void Predictor::sequenceReset()
    {
        learner->sequenceReset();
    }

    void Predictor::newLearner(const LearnerType learnerType)
    {
        switch (learnerType)
        {
        case LearnerType::MCMCLearner:
            newMCMCLearner();
            break;
        case LearnerType::MCNetLearner:
            newMCNetLearner();
            break;
        case LearnerType::TDNetLearner:
            newTDNetLearner();
            break;
        }
    }

    LearnerType Predictor::getLearnerType() const
    {
        return learnerType;
    }

    bool Predictor::isLearnerType(const LearnerType learnerType) const
    {
        return (learnerType == this->learnerType);
    }

    void Predictor::newMCMCLearner(unsigned int stateLength,
            unsigned int predictionLength, const double learnRate,
            const double discountRate, const double defaultValue)
    {
        if (stateLength == 0) stateLength = getStateLength();
        if (predictionLength == 0) predictionLength = getPredictionLength();
        delete learner;
        learner = new MCMCLearner(stateLength, predictionLength, learnRate,
                discountRate, defaultValue);
        learnerType = LearnerType::MCMCLearner;
}

    void Predictor::newMCNetLearner(Vector layerSize,
            const NeuronType hiddenType, const double learnRate,
            const double discountRate, const double gamma)
    {
        if (layerSize.size() == 0)
        {
            layerSize = Vector(3);
            layerSize[0] = getStateLength();
            layerSize[1] = getStateLength() * 2;
            layerSize[2] = getPredictionLength();
        }
        delete learner;
        NetLearner* netLearner = new MCNetLearner(layerSize, learnRate,
                discountRate, gamma);
        netLearner->setHiddenNeuronType(hiddenType);
        learner = netLearner;
        learnerType = LearnerType::MCNetLearner;
    }

    void Predictor::newTDNetLearner(Vector layerSize,
            const NeuronType hiddenType, const double learnRate,
            const double discountRate, const double gamma, const double lambda)
    {
        if (layerSize.size() == 0)
        {
            layerSize = Vector(3);
            layerSize[0] = getStateLength();
            layerSize[1] = getStateLength() * 2;
            layerSize[2] = getPredictionLength();
        }
        delete learner;
        NetLearner* netLearner = new TDNetLearner(layerSize, learnRate,
                discountRate, gamma, lambda);
        netLearner->setHiddenNeuronType(hiddenType);
        learner = netLearner;
        learnerType = LearnerType::TDNetLearner;
    }

    bool Predictor::loadLearner(const char* filename)
    {
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.good())
        {
            Str networkType = getNext(fileStream);
            if (networkType == "MCMCLearner")
            {
                return loadMCMCLearner(fileStream);
            }
            if (networkType == "MCNetLearner")
            {
                return loadMCNetLearner(fileStream);
            }
            if (networkType == "TDNetLearner")
            {
                return loadTDNetLearner(fileStream);
            }
        }
        return false;
    }

    bool Predictor::saveLearner(const char* filename)
    {
        if (learner != 0)
        {
            std::ofstream fileStream(filename, std::ofstream::out);
            if (fileStream.is_open())
            {
                MCMCLearner* mcmcLearner =
                        dynamic_cast<MCMCLearner*>(learner);
                MCNetLearner* mcNetLearner =
                        dynamic_cast<MCNetLearner*>(learner);
                TDNetLearner* tdNetLearner =
                        dynamic_cast<TDNetLearner*>(learner);
                if (mcmcLearner)
                {
                    fileStream << "MCMCLearner" << std::endl;
                    fileStream << *mcmcLearner;
                }
                if (mcNetLearner)
                {
                    fileStream << "MCNetLearner" << std::endl;
                    fileStream << *mcNetLearner;
                }
                if (tdNetLearner)
                {
                    fileStream << "TDNetLearner" << std::endl;
                    fileStream << *tdNetLearner;
                }
                fileStream << std::endl;
            }
            if (fileStream.good())
            {
                return true;
            }
        }
        return false;
    }

    void Predictor::setLearning(bool learning)
    {
        this->learning = learning;
    }

    bool Predictor::isLearning() const
    {
        return learning;
    }

    unsigned int Predictor::getStateLength() const
    {
        return learner->getStateLength();
    }

    unsigned int Predictor::getPredictionLength() const
    {
        return learner->getOutputLength();
    }

    void Predictor::adjustPredictor(const unsigned int stateLength,
            const unsigned int predictionLength)
    {
        if (stateLength > 0 && predictionLength > 0)
        {
            MCMCLearner* mcmcLearner =
                    dynamic_cast<MCMCLearner*>(learner);
            NetLearner* netLearner =
                    dynamic_cast<NetLearner*>(learner);
            if (mcmcLearner)
            {
                mcmcLearner->adjustLearner(stateLength, predictionLength);
            }
            if (netLearner)
            {
                unsigned int layers =
                        netLearner->getNetwork().getLayers();
                Vector layerSize =
                        netLearner->getNetwork().getLayerSizes();
                layerSize[0] = stateLength;
                layerSize[layers-1] = predictionLength;
                netLearner->adjustNetwork(layerSize);
            }
        }
    }

    double Predictor::getLearningRate() const
    {
        return learner->getLearningRate();
    }

    void Predictor::setLearningRate(const double learningRate)
    {
        learner->setLearningRate(learningRate);
    }

    double Predictor::getDiscountRate() const
    {
        return learner->getDiscountRate();
    }

    void Predictor::setDiscountRate(const double discountRate)
    {
        learner->setDiscountRate(discountRate);
    }

    Vector Predictor::getLayerSizes() const
    {
        NetLearner* netLearner = dynamic_cast<NetLearner*>(learner);
        if (!netLearner) return Vector();
        return netLearner->getLayerSizes();
    }

    double Predictor::getRegularisationFactor() const
    {
        NetLearner* netLearner = dynamic_cast<NetLearner*>(learner);
        if (!netLearner) return 0.001;
        return netLearner->getRegularisationFactor();
    }

    void Predictor::setRegularisationFactor(const double gamma)
    {
        NetLearner* netLearner = dynamic_cast<NetLearner*>(learner);
        if (!netLearner) return;
        netLearner->setRegularisationFactor(gamma);
    }

    double Predictor::getLambda() const
    {
        TDNetLearner* tdNetLearner = dynamic_cast<TDNetLearner*>(learner);
        if (!tdNetLearner) return 0.7;
        return tdNetLearner->getLambda();
    }

    void Predictor::setLambda(const double lambda)
    {
        TDNetLearner* tdNetLearner = dynamic_cast<TDNetLearner*>(learner);
        if (!tdNetLearner) return;
        tdNetLearner->setLambda(lambda);
    }

    bool Predictor::loadMCMCLearner(std::istream& is)
    {
        MCMCLearner* mcmcLearner = new MCMCLearner();
        is >> *mcmcLearner;
        if (is.good() &&
            learner->getStateLength() == mcmcLearner->getStateLength() &&
            learner->getOutputLength() == mcmcLearner->getOutputLength())
        {
            delete learner;
            learner = mcmcLearner;
            return true;
        }
        else
        {
            delete mcmcLearner;
            return false;
        }
    }

    bool Predictor::loadMCNetLearner(std::istream& is)
    {
        MCNetLearner* mcNetLearner = new MCNetLearner();
        is >> *mcNetLearner;
        if (is.good() &&
            learner->getStateLength() == mcNetLearner->getStateLength() &&
            learner->getOutputLength() == mcNetLearner->getOutputLength())
        {
            delete learner;
            learner = mcNetLearner;
            return true;
        }
        else
        {
            delete mcNetLearner;
            return false;
        }
    }

    bool Predictor::loadTDNetLearner(std::istream& is)
    {
        TDNetLearner* tdNetLearner = new TDNetLearner();
        is >> *tdNetLearner;
        if (is.good() &&
            learner->getStateLength() == tdNetLearner->getStateLength() &&
            learner->getOutputLength() == tdNetLearner->getOutputLength())
        {
            delete learner;
            learner = tdNetLearner;
            return true;
        }
        else
        {
            delete tdNetLearner;
            return false;
        }
    }
}
