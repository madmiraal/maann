// (c) 2016 - 2017: Marcel Admiraal

#include "netlearner.h"

#include "batchtrainnetwork.h"

namespace ma
{
    NetLearner::NetLearner(const unsigned int stateLength,
            const unsigned int outputLength, const double learnRate,
            const double discountRate, const double gamma) :
        Learner(stateLength, outputLength, learnRate, discountRate),
        gamma(gamma)
    {
        // Create a default network.
        unsigned int layerArray[] = {stateLength, stateLength*2, outputLength};
        Vector layerSize(layerArray, 3);
        network.create(layerSize);
    }

    NetLearner::NetLearner(const Vector& layerSize, const double learnRate,
            const double discountRate, const double gamma,
            const NeuronType type) :
        Learner(layerSize[0], layerSize[layerSize.size()-1],
                learnRate, discountRate),
        gamma(gamma)
    {
        network.create(layerSize, type);
    }

    NetLearner::NetLearner(const NeuralNetwork& network, const double learnRate,
            const double discountRate, const double gamma) :
        Learner(network.getLayerSizes()[0],
                network.getLayerSizes()[network.getLayerSizes().size()-1],
                learnRate, discountRate),
        network(network), gamma(gamma)
    {
    }

    NetLearner::NetLearner(const NetLearner& source) :
        Learner(source), network(source.network), gamma(source.gamma)
    {
    }

    NetLearner::~NetLearner()
    {
    }

    void swap(NetLearner& first, NetLearner& second)
    {
        swap(static_cast<Learner&>(first), static_cast<Learner&>(second));
        swap(first.network, second.network);
        std::swap(first.gamma, second.gamma);
    }

    Vector NetLearner::getStateValue(const Vector& state)
    {
        return network.getOutput(state);
    }

    void NetLearner::adjustLearner(const unsigned int stateLength,
            const unsigned int outputLength)
    {
        Vector layerSize = network.getLayerSizes();
        layerSize[0] = stateLength;
        layerSize[layerSize.size()-1] = outputLength;
        adjustNetwork(layerSize);
    }

    NeuralNetwork NetLearner::getNetwork() const
    {
        return network;
    }

    void NetLearner::setNetwork(const NeuralNetwork network)
    {
        Vector layerSize = network.getLayerSizes();
        this->stateLength = layerSize[0];
        this->outputLength = layerSize[layerSize.size()-1];
        this->network = network;
        sequenceReset();
    }

    Vector NetLearner::getLayerSizes() const
    {
        return network.getLayerSizes();
    }

    void NetLearner::createNetwork(const Vector& layerSize)
    {
        Learner::adjustLearner(layerSize[0], layerSize[layerSize.size()-1]);
        network.create(layerSize);
        sequenceReset();
    }

    void NetLearner::adjustNetwork(const Vector& layerSize)
    {
        Learner::adjustLearner(layerSize[0], layerSize[layerSize.size()-1]);
        network.setLayerSizes(layerSize);
        sequenceReset();
    }

    bool NetLearner::setHiddenNeuronType(const NeuronType hiddenNeuronType)
    {
        if (hiddenNeuronType != NeuronType::Logistic &&
            hiddenNeuronType != NeuronType::Threshold) return false;
        for (unsigned int layerIndex = 1; layerIndex < network.getLayers() - 1;
                ++layerIndex)
        {
            network.setNeuronType(layerIndex, hiddenNeuronType);
        }
        return true;
    }

    double NetLearner::getRegularisationFactor() const
    {
        return gamma;
    }

    void NetLearner::setRegularisationFactor(const double gamma)
    {
        this->gamma = gamma;
    }
}
