// (c) 2016 - 2017: Marcel Admiraal

#include "mcnetlearner.h"

namespace ma
{
    MCNetLearner::MCNetLearner(const unsigned int stateLength,
            const unsigned int outputLength, const double learnRate,
            const double discountRate, const double gamma) :
        NetLearner(stateLength, outputLength, learnRate, discountRate, gamma)

    {
    }

    MCNetLearner::MCNetLearner(const Vector& layerSize, const double learnRate,
            const double discountRate, const double gamma) :
        NetLearner(layerSize, learnRate, discountRate, gamma)
    {
    }

    MCNetLearner::MCNetLearner(const NeuralNetwork& network,
            const double learnRate, const double discountRate,
            const double gamma) :
        NetLearner(network, learnRate, discountRate, gamma)
    {
    }

    MCNetLearner::MCNetLearner(const MCNetLearner& source) :
        NetLearner(source), sequence(source.sequence)
    {
    }

    MCNetLearner::MCNetLearner(const char* filename) :
        NetLearner()
    {
        load(filename);
    }

    MCNetLearner::~MCNetLearner()
    {
    }

    void swap(MCNetLearner& first, MCNetLearner& second)
    {
        swap(static_cast<NetLearner&>(first), static_cast<NetLearner&>(second));
        swap(first.sequence, second.sequence);
    }

    MCNetLearner& MCNetLearner::operator=(MCNetLearner source)
    {
        swap(*this, source);
        return *this;
    }

    bool MCNetLearner::save(const char* filename) const
    {
        std::ofstream fileStream(filename, std::ofstream::out);
        if (fileStream.is_open())
        {
            fileStream << *this;
            fileStream << std::endl;
        }
        if (fileStream.good())
        {
            return true;
        }
        return false;
    }

    bool MCNetLearner::load(const char* filename)
    {
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.good())
        {
            MCNetLearner result;
            fileStream >> result;
            if (fileStream.good())
            {
                swap(*this, result);
                return true;
            }
        }
        return false;
    }

    void MCNetLearner::nextState(const Vector& state, const Vector& reward)
    {
        updateRewards(reward);
        // Add this state to the sequence.
        std::tuple<Vector, Vector> step(state, Vector(outputLength));
        sequence.append(step);
    }

    void MCNetLearner::finalReward(const Vector& reward)
    {
        updateRewards(reward);
        for (Iterator<std::tuple<Vector, Vector> > sequenceIter =
                sequence.first(); sequenceIter.valid(); ++sequenceIter)
        {
            std::tuple<Vector, Vector> stateReward = *sequenceIter;
            Vector state = std::get<0>(stateReward);
            Vector target = std::get<1>(stateReward);
            Vector gradient  = network.getCostGradients(target, state, gamma);
            Vector weight = network.getWeights();
            weight -= gradient * learnRate;
            network.setWeights(weight);
        }
        sequenceReset();
    }

    void MCNetLearner::sequenceReset()
    {
        sequence.clear();
    }

    void MCNetLearner::updateRewards(const Vector& reward)
    {
        if (reward.size() != outputLength) return;
        Vector discountedReward(reward);
        for (Iterator<std::tuple<Vector, Vector> > sequenceIter =
                sequence.last(); sequenceIter.valid(); --sequenceIter)
        {
            std::tuple<Vector, Vector> sequenceStateReward = *sequenceIter;
            Vector sequenceState = std::get<0>(sequenceStateReward);
            Vector sequenceReward = std::get<1>(sequenceStateReward);
            sequenceReward += discountedReward;
            std::tuple<Vector, Vector> newStateReward(
                    sequenceState, sequenceReward);
            sequenceIter.setData(newStateReward);
            discountedReward*= discountRate;
        }
    }

    std::istream& operator>>(std::istream& is, MCNetLearner& destination)
    {
        double learnRate;
        double discountRate;
        double gamma;
        NeuralNetwork network;
        is >> learnRate;
        is >> discountRate;
        is >> gamma;
        is >> network;
        if (is.good())
        {
            destination.setLearningRate(learnRate);
            destination.setDiscountRate(discountRate);
            destination.setRegularisationFactor(gamma);
            destination.setNetwork(network);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const MCNetLearner& source)
    {
        os << source.getLearningRate() << '\t';
        os << source.getDiscountRate() << '\t';
        os << source.getRegularisationFactor() << '\t';
        os << source.getNetwork();
        return os;
    }
}
