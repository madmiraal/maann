// (c) 2015 - 2017: Marcel Admiraal

#include "neuron.h"

#include <cmath>
#include <cstdlib>
#include <cstring>

namespace ma
{
    double modify(const double raw, const NeuronType type)
    {
        switch(type)
        {
        case NeuronType::Normal:
            return raw;
        case NeuronType::Binary:
            return (raw >= 0.0) ? 1.0 : 0.0;
        case NeuronType::Threshold:
            return (raw >= 0.0) ? raw : 0.0;
        case NeuronType::Logistic:
            return 1 / (1 + exp(-(raw)));
        case NeuronType::HyperbolicTangent:
        {
            double e = exp(2 * raw);
            return (e - 1) / (e + 1);
        }
        case NeuronType::StochasticBinary:
        {
            double p = 1.0 * rand() / RAND_MAX;
            double sigmoid = 1 / (1 + exp(-(raw)));
            return (sigmoid > p) ? 1.0 : 0.0;
        }
        default:
            return 0;
        }
    }

    double cost(const double output, const double actual, const NeuronType type)
    {
        switch(type)
        {
        case NeuronType::Normal:
            return (output - actual) * (output - actual) / 2;
        case NeuronType::Binary:
            throw "Unknown cost";
        case NeuronType::Threshold:
            return (output - actual) * (output - actual) / 2;
        case NeuronType::Logistic:
        case NeuronType::HyperbolicTangent:
        {
            double result = 0;
            if (actual == 1)
                result = -log(output);
            else if (actual == 0)
                result = -log(1 - output);
            else
            {
                result -= actual * log(output);
                result -= (1 - actual) * log(1 - output);
            }
            return result;
        }
        case NeuronType::StochasticBinary:
            throw "Unknown cost";
        default:
            throw "Unknown cost";
        }
    }

    double derivative(const double output, const NeuronType type,
            const bool raw)
    {
        double modifiedOutput = output;
        if (raw)
        {
            modifiedOutput = modify(output, type);
        }
        switch(type)
        {
        case NeuronType::Normal:
            return 1.0;
        case NeuronType::Binary:
            throw "Unknown derivative";
        case NeuronType::Threshold:
            if (output < 0) return 0.0;
            return 1.0;
        case NeuronType::Logistic:
            return output * (1 - modifiedOutput);
        case NeuronType::HyperbolicTangent:
            return 1 - (modifiedOutput * modifiedOutput);
        case NeuronType::StochasticBinary:
            throw "Unknown derivative";
        default:
            throw "Unknown derivative";
        }
    }

    void print(const NeuronType type, std::ostream& os)
    {
        switch(type)
        {
        case NeuronType::Normal:
            os << "Normal neuron";
            break;
        case NeuronType::Binary:
            os << "Binary neuron";
            break;
        case NeuronType::Threshold:
            os << "Threshold neuron";
            break;
        case NeuronType::Logistic:
            os << "Logistic neuron";
            break;
        case NeuronType::HyperbolicTangent:
            os << "Hyperbolic tangent neuron";
            break;
        case NeuronType::StochasticBinary:
            os << "Stochastic binary neuron";
            break;
        default:
            os << "Unknown neuron type";
        }
    }
}
