// (c) 2015 - 2017: Marcel Admiraal

#include "neuralnetwork.h"

#include <cmath>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <fstream>
#include <sstream>

namespace ma
{
    NeuralNetwork::NeuralNetwork(const Vector& layerSize, const NeuronType type)
    {
        build(layerSize, type);
    }

    NeuralNetwork::~NeuralNetwork()
    {
        destroy();
    }

    NeuralNetwork::NeuralNetwork(const NeuralNetwork& source) :
            layerSize(source.layerSize), layers(source.layers),
            weights(source.weights)
    {
        if (layers > 0)
        {
            build(layerSize);
            for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
            {
                for (unsigned int inputIndex = 0;
                        inputIndex < layerSize[layerIndex-1]+1; ++inputIndex)
                {
                    neuronValue[layerIndex-1][inputIndex] =
                            source.neuronValue[layerIndex-1][inputIndex];
                }
                for (unsigned int neuronIndex = 0;
                        neuronIndex < layerSize[layerIndex]; ++neuronIndex)
                {
                    for (unsigned int inputIndex = 0;
                            inputIndex < layerSize[layerIndex-1]+1;
                            ++inputIndex)
                    {
                        weight[layerIndex-1][neuronIndex][inputIndex] =
                                source.weight[layerIndex-1][neuronIndex][inputIndex];
                    }
                    neuronType[layerIndex-1][neuronIndex] =
                            source.neuronType[layerIndex-1][neuronIndex];
                }
            }
            for (unsigned int neuronIndex = 0;
                    neuronIndex < layerSize[layers-1]; ++neuronIndex)
            {
                neuronValue[layers-1][neuronIndex] =
                        source.neuronValue[layers-1][neuronIndex];
            }
        }
    }

    NeuralNetwork::NeuralNetwork(const char* filename)
    {
        build();
        load(filename);
    }

    void swap(NeuralNetwork& first, NeuralNetwork& second)
    {
        swap(first.layerSize, second.layerSize);
        std::swap(first.layers, second.layers);
        std::swap(first.weights, second.weights);
        std::swap(first.weight, second.weight);
        std::swap(first.neuronValue, second.neuronValue);
        std::swap(first.error, second.error);
        std::swap(first.neuronType, second.neuronType);
    }

    NeuralNetwork& NeuralNetwork::operator=(NeuralNetwork source)
    {
        swap(*this, source);
        return *this;
    }

    void NeuralNetwork::create(const Vector& layerSize, const NeuronType type)
    {
        destroy();
        build(layerSize, type);
    }

    unsigned int NeuralNetwork::getParameters() const
    {
        return weights;
    }

    unsigned int NeuralNetwork::getLayers() const
    {
        return layers;
    }

    Vector NeuralNetwork::getLayerSizes() const
    {
        return layerSize;
    }

    void NeuralNetwork::setLayerSizes(const Vector& layerSize)
    {
        if (layerSize.size() != layers)
        {
            create(layerSize);
            return;
        }
        NeuralNetwork newNetwork(layerSize);
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            for (unsigned int inputIndex = 0;
                    inputIndex < layerSize[layerIndex-1]+1 &&
                    inputIndex < this->layerSize[layerIndex-1]+1; ++inputIndex)
            {
                newNetwork.neuronValue[layerIndex-1][inputIndex] =
                        neuronValue[layerIndex-1][inputIndex];
            }
            for (unsigned int neuronIndex = 0;
                    neuronIndex < layerSize[layerIndex] &&
                    neuronIndex < this->layerSize[layerIndex]; ++neuronIndex)
            {
                for (unsigned int inputIndex = 0;
                        inputIndex < layerSize[layerIndex-1]+1 &&
                        inputIndex < this->layerSize[layerIndex-1]+1;
                        ++inputIndex)
                {
                    newNetwork.weight[layerIndex-1][neuronIndex][inputIndex] =
                            weight[layerIndex-1][neuronIndex][inputIndex];
                }
                newNetwork.neuronType[layerIndex-1][neuronIndex] =
                        neuronType[layerIndex-1][neuronIndex];
            }
        }
        for (unsigned int neuronIndex = 0;
                neuronIndex < layerSize[layers-1] &&
                neuronIndex < this->layerSize[layers-1]; ++neuronIndex)
        {
            newNetwork.neuronValue[layers-1][neuronIndex] =
                    neuronValue[layers-1][neuronIndex];
        }
        swap(newNetwork, *this);
    }

    unsigned int NeuralNetwork::getLayerSize(const unsigned int layerIndex) const
    {
        if (layerIndex >= layers) return 0;
        return layerSize[layerIndex];
    }

    void NeuralNetwork::setLayerSize(const unsigned int layerIndex,
            const unsigned int layerSize)
    {
        Vector newLayerSize = getLayerSizes();
        if (layerIndex > layers - 1)
        {
            newLayerSize.extend(layerIndex - layers);
        }
        newLayerSize[layerIndex] = layerSize;
        setLayerSizes(newLayerSize);
    }

    Vector NeuralNetwork::getWeights() const
    {
        Vector weightVector(weights);
        unsigned int parameter = 0;
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            for (unsigned int neuronIndex = 0;
                    neuronIndex < layerSize[layerIndex]; ++neuronIndex)
            {
                for (unsigned int inputIndex = 0;
                        inputIndex < layerSize[layerIndex-1]+1; ++inputIndex)
                {
                    weightVector[parameter] =
                            weight[layerIndex-1][neuronIndex][inputIndex];
                    ++parameter;
                }
            }
        }
        return weightVector;
    }

    Vector NeuralNetwork::getNonBiasWeights() const
    {
        Vector weightVector = getWeights();
        // Set bias weights to zero.
        unsigned int parameter = 0;
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            for (unsigned int neuronIndex = 0;
                    neuronIndex < layerSize[layerIndex]; ++neuronIndex)
            {
                weightVector[parameter] = 0;
                parameter += layerSize[layerIndex-1]+1;
            }
        }
        return weightVector;
    }

    double NeuralNetwork::getWeight(const unsigned int layerIndex,
            const unsigned int neuronIndex, const unsigned int inputIndex) const
    {
        if (layerIndex == 0 || layerIndex >= layers ||
                neuronIndex >= layerSize[layerIndex] ||
                inputIndex >= layerSize[layerIndex-1]+1) return 0;
        return weight[layerIndex-1][neuronIndex][inputIndex];
    }

    bool NeuralNetwork::setWeights(const Vector& weightVector)
    {
        if (weightVector.size() != weights) return false;
        unsigned int parameter= 0;
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            for (unsigned neuronIndex = 0; neuronIndex < layerSize[layerIndex];
                    ++neuronIndex)
            {
                for (unsigned int inputIndex = 0;
                        inputIndex < layerSize[layerIndex-1]+1; ++inputIndex)
                {
                    weight[layerIndex-1][neuronIndex][inputIndex] =
                            weightVector[parameter];
                    ++parameter;
                }
            }
        }
        return true;
    }

    bool NeuralNetwork::setWeight(const unsigned int layerIndex,
            const unsigned int neuronIndex, const unsigned int inputIndex,
            const double value)
    {
        if (layerIndex == 0 || layerIndex >= layers ||
                neuronIndex >= layerSize[layerIndex] ||
                inputIndex >= layerSize[layerIndex-1]+1) return false;
        weight[layerIndex-1][neuronIndex][inputIndex] = value;
        return true;
    }

    void NeuralNetwork::initialiseWeights()
    {
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            double epsilon = sqrt(6) /
                    sqrt(layerSize[layerIndex] * layerSize[layerIndex-1]);
            for (unsigned int neuronIndex = 0;
                    neuronIndex < layerSize[layerIndex]; ++neuronIndex)
            {
                for (unsigned int inputIndex = 0;
                        inputIndex < layerSize[layerIndex-1]+1; ++inputIndex)
                {
                    weight[layerIndex-1][neuronIndex][inputIndex] =
                        ((rand() * 1.0 / RAND_MAX) * 2 * epsilon) - epsilon;
                }
            }
        }
    }

    Vector NeuralNetwork::getBiases() const
    {
        Vector result(layers - 1);
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            result[layerIndex-1] = (neuronValue[layerIndex-1][0] == 0) ? 0 : 1;
        }
        return result;
    }

    bool NeuralNetwork::setBiases(const Vector& bias)
    {
        if (bias.size() != layers -1) return false;
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            setBias(layerIndex, (bias[layerIndex-1] != 0));
        }
        return true;
    }

    void NeuralNetwork::setBiases(const bool use)
    {
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            setBias(layerIndex, use);
        }
    }

    bool NeuralNetwork::setBias(const unsigned int layerIndex,
            const bool use)
    {
        if (layerIndex == 0 || layerIndex >= layers) return false;
        if (use)
            neuronValue[layerIndex-1][0] = 1.0;
        else
            neuronValue[layerIndex-1][0] = 0.0;
        return true;
    }

    NeuronType NeuralNetwork::getNeuronType(const unsigned int layerIndex,
            const unsigned int neuronIndex) const
    {
        return neuronType[layerIndex-1][neuronIndex];
    }

    bool NeuralNetwork::setNeuronType(const unsigned int layerIndex,
            const unsigned int neuronIndex, NeuronType newNeuronType) const
    {
        if (layerIndex == 0 || layerIndex >= layers ||
                neuronIndex >= layerSize[layerIndex]) return false;
        neuronType[layerIndex-1][neuronIndex] = newNeuronType;
        return true;
    }

    bool NeuralNetwork::setNeuronType(const unsigned int layerIndex,
            NeuronType newNeuronType) const
    {
        if (layerIndex == 0 || layerIndex >= layers) return false;
        for (unsigned int neuronIndex = 0; neuronIndex < layerSize[layerIndex];
                ++neuronIndex)
        {
            setNeuronType(layerIndex, neuronIndex, newNeuronType);
        }
        return true;
    }

    void NeuralNetwork::setNeuronType(NeuronType newNeuronType) const
    {
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            setNeuronType(layerIndex, newNeuronType);
        }
    }

    Vector NeuralNetwork::getNeuronInputs() const
    {
        if (layers == 1)
        {
            // output = input; so no bias created.
            return getNeuronOutputs();
        }
        unsigned int inputs = 0;
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            inputs += layerSize[layerIndex-1]+1;
        }
        Vector input(inputs);
        unsigned int parameter = 0;
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            for (unsigned int inputIndex = 0;
                    inputIndex < layerSize[layerIndex-1]+1; ++inputIndex)
            {
                input[parameter] =
                        neuronValue[layerIndex-1][inputIndex];
                ++parameter;
            }
        }
        return input;
    }

    Vector NeuralNetwork::getNeuronInputs(const unsigned int layerIndex) const
    {
        if (layerIndex == 0 || layerIndex >= layers) return Vector();
        Vector input(neuronValue[layerIndex-1], layerSize[layerIndex-1]+1);
        return input;
    }

    Vector NeuralNetwork::getNeuronOutputs() const
    {
        if (layers == 1)
        {
            // output = input; so no bias created.
            Vector output(neuronValue[layers-1], layerSize[layers-1]);
            return output;
        }

        unsigned int outputs = 0;
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            outputs += layerSize[layerIndex];
        }
        Vector output(outputs);
        unsigned int parameter = 0;
        for (unsigned int layerIndex = 1; layerIndex < layers-1; ++layerIndex)
        {
            for (unsigned int outputIndex = 0;
                    outputIndex < layerSize[layerIndex]; ++outputIndex)
            {
                output[parameter] =
                        neuronValue[layerIndex][outputIndex+1]; // Skip bias.
                ++parameter;
            }
        }
        // Output layer doesn't have a bias.
        for (unsigned int outputIndex = 0;
                outputIndex < layerSize[layers-1]; ++outputIndex)
        {
            output[parameter] =
                    neuronValue[layers-1][outputIndex];
            ++parameter;
        }
        return output;
    }

    Vector NeuralNetwork::getNeuronOutputs(const unsigned int layerIndex) const
    {
        if (layerIndex == 0 && layers == 1)
        {
            // Output = input.
            return Vector(neuronValue[layerIndex], layerSize[layerIndex]);
        }
        if (layerIndex == 0 || layerIndex >= layers)
        {
            // Invalid index.
            return Vector();
        }
        if (layerIndex == layers-1)
        {
            // No bias.
            return Vector(neuronValue[layerIndex], layerSize[layerIndex]);
        }
        // Ignore the bias.
        return Vector(&neuronValue[layerIndex][1], layerSize[layerIndex]);
    }

    Vector NeuralNetwork::getOutput(const Vector& input)
    {
        calculateOutput(input);
        // Get and return the output.
        Vector output = getNeuronOutputs(layers-1);
        return output;
    }

    double NeuralNetwork::getCost(const Vector& target, const Vector& input,
            const double gamma)
    {
        if (target.size() != layerSize[layers-1]) return 0;
        calculateOutput(input);

        if (layers == 1)
        {
            // If a single layer simply return the root difference square sum.
            return sqrt((target - getOutput()).elementSquaredSum());
        }

        double costSum = 0.0;
        unsigned int outputNeurons = layerSize[layers-1];
        for (unsigned int neuronIndex = 0; neuronIndex < outputNeurons;
                ++neuronIndex)
        {
            costSum += ma::cost(neuronValue[layers-1][neuronIndex],
                    target[neuronIndex], neuronType[layers-2][neuronIndex]);
        }
        if (gamma != 0)
        {
            double regularisation = getNonBiasWeights().elementSquaredSum();
            costSum += gamma * regularisation / 2;
        }
        return costSum;
    }

    Vector NeuralNetwork::getErrors(const unsigned int layerIndex,
            const Vector& target, const Vector& input)
    {
        calculateOutput(input);
        backPropagateErrors(target);
        if (layerIndex == 0)
        {
            if (layers == 1)
            {
                Vector errorVector(error[0], layerSize[0]);
                return errorVector;
            }
            return Vector(layerSize[0]);
        }
        return Vector(error[layerIndex-1], layerSize[layerIndex]);
    }

    Vector NeuralNetwork::getErrors(const Vector& target, const Vector& input)
    {
        calculateOutput(input);
        backPropagateErrors(target);
        if (layers == 1)
        {
            return getErrors(0);
        }
        else
        {
            Vector errorVector;
            for (unsigned int layerIndex = 1; layerIndex < layers;
                    ++layerIndex)
            {
                errorVector.concatenate(getErrors(layerIndex));
            }
            return errorVector;
        }
    }

    Vector NeuralNetwork::getCostGradients(const Vector& target,
            const Vector& input, const double gamma)
    {
        calculateOutput(input);
        backPropagateErrors(target);
        Vector costGradient(weights);
        unsigned int parameter = 0;
        unsigned int upperSize = layerSize[0];
        for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
        {
            unsigned int lowerSize = upperSize + 1;
            upperSize = layerSize[layerIndex];
            double* thisNeuronValue = neuronValue[layerIndex-1];
            double* thisError = error[layerIndex-1];
            // Gradient is the outer product of the input and the output deltas.
            for (unsigned int neuronIndex = 0;
                    neuronIndex < upperSize; ++neuronIndex)
            {
                for (unsigned int inputIndex = 0;
                        inputIndex < lowerSize; ++inputIndex)
                {
                    costGradient[parameter] =
                            thisNeuronValue[inputIndex] *
                            thisError[neuronIndex];
                    ++parameter;
                }
            }
        }
        if (gamma != 0)
        {
            Vector regularisation = getNonBiasWeights();
            costGradient += gamma * regularisation;
        }
        return costGradient;
    }

    void NeuralNetwork::print(std::ostream& os) const
    {
        std::cout << "Layers = " << layers << ": ";
        for (unsigned int layerIndex = 0; layerIndex < layers; ++layerIndex)
        {
            std::cout << layerSize[layerIndex] << "\t";
        }
        std::cout << std::endl;
        for (unsigned int layerIndex = 0; layerIndex < layers; ++layerIndex)
        {
            if (layerIndex > 0)
            {
                for (unsigned int neuronIndex = 0;
                        neuronIndex < layerSize[layerIndex]; ++neuronIndex)
                {
                    for (unsigned int inputIndex = 0;
                            inputIndex < layerSize[layerIndex-1]+1; ++inputIndex)
                    {
                        std::cout << weight[layerIndex-1][neuronIndex][inputIndex];
                        std::cout << "\t";
                    }
                    std::cout << std::endl;
                }
                for (unsigned int neuronIndex = 0;
                        neuronIndex < layerSize[layerIndex]; ++neuronIndex)
                {
                    ma::print(neuronType[layerIndex-1][neuronIndex], std::cout);
                    std::cout << " ";
                }
                std::cout << std::endl;
            }
            std::cout << "Layer[" << layerIndex << "]: ";
            if (layerIndex == layers-1)
            {
                for (unsigned int neuronIndex = 0;
                        neuronIndex < layerSize[layerIndex]; ++neuronIndex)
                {
                    std::cout << neuronValue[layerIndex][neuronIndex] << "\t";
                }
            }
            else
            {
                for (unsigned int neuronIndex = 0;
                        neuronIndex < layerSize[layerIndex]+1; ++neuronIndex)
                {
                    std::cout << neuronValue[layerIndex][neuronIndex] << "\t";
                }
            }
            std::cout << std::endl;
        }
    }

    bool NeuralNetwork::save(const char* filename) const
    {
        std::ofstream fileStream(filename, std::ofstream::out);
        if (fileStream.is_open())
        {
            fileStream << *this;
            fileStream << std::endl;
        }
        if (fileStream.good())
        {
            return true;
        }
        return false;
    }

    bool NeuralNetwork::load(const char* filename)
    {
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.good())
        {
            NeuralNetwork result;
            fileStream >> result;
            if (fileStream.good())
            {
                swap(*this, result);
                return true;
            }
        }
        return false;
    }

    void NeuralNetwork::build(const Vector& layerSize, NeuronType type)
    {
        layers = layerSize.size();
        weights = 0;
        if (layers > 0)
        {
            this->layerSize = layerSize;
            weight = new double**[layers-1];
            neuronValue = new double*[layers];
            if (layers == 1)
            {
                error = new double*[1];
                error[0] = new double[(unsigned int)layerSize[0]];
            }
            else
            {
                error = new double*[layers-1];
            }
            neuronType = new NeuronType*[layers-1];
            unsigned int upperSize = layerSize[0];
            for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
            {
                unsigned int lowerSize = upperSize;
                upperSize = layerSize[layerIndex];
                weights += upperSize * (lowerSize+1);
                weight[layerIndex-1] = new double*[upperSize];
                neuronValue[layerIndex-1] = new double[lowerSize+1](); // + bias;
                neuronValue[layerIndex-1][0] = 1; // Set bias as default.
                error[layerIndex-1] = new double[upperSize]();
                neuronType[layerIndex-1] = new NeuronType[upperSize];
                for (unsigned int neuronIndex = 0; neuronIndex < upperSize;
                        ++neuronIndex)
                {
                    weight[layerIndex-1][neuronIndex] = new double[lowerSize+1]();
                    neuronType[layerIndex-1][neuronIndex] = type;
                }
            }
            // Output layer does not have a bias.
            upperSize = layerSize[layers-1];
            neuronValue[layers-1] = new double[upperSize]();
        }
        initialiseWeights();
    }

    void NeuralNetwork::destroy()
    {
        if (layers > 0)
        {
            delete[] neuronValue[layers-1];
            for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
            {
                delete[] neuronType[layerIndex-1];
                delete[] error[layerIndex-1];
                delete[] neuronValue[layerIndex-1];
                for (unsigned int neuronIndex = 0;
                        neuronIndex < layerSize[layerIndex]; ++neuronIndex)
                {
                    delete[] weight[layerIndex-1][neuronIndex];
                }
                delete[] weight[layerIndex-1];
            }
            delete[] neuronType;
            if (layers == 1) delete[] error[0];
            delete[] error;
            delete[] neuronValue;
            delete[] weight;
        }
        weights = 0;
        layers = 0;
    }

    bool NeuralNetwork::setInput(const Vector& input)
    {
        if (input.size() != layerSize[0]) return false;
        if (layers == 1)
        {
            // No bias if a single layer.
            input.getArray(neuronValue[0]);
        }
        else
        {
            // Skip bias.
            input.getArray(&neuronValue[0][1]);
        }
        return true;
    }

    bool NeuralNetwork::calculateOutput(const Vector& input)
    {
        if (!setInput(input)) return false;
        // If a single layer, output equals input.
        if (layers == 1) return true;
        // Calculate each layer's output.
        unsigned int upperSize = layerSize[0];
        for (unsigned int layerIndex = 1; layerIndex < layers-1; ++layerIndex)
        {
            unsigned int lowerSize = upperSize + 1;
            upperSize = layerSize[layerIndex];
            double* inputNeuronValue = neuronValue[layerIndex-1];
            double* outputNeuronValue = neuronValue[layerIndex];
            double** inputNeuronWeight = weight[layerIndex-1];
            NeuronType* thisNeuronType = neuronType[layerIndex-1];
            for (unsigned int neuronIndex = 0;
                    neuronIndex < upperSize; ++neuronIndex)
            {
                double thisNeuronValue = 0;
                double* thisWeight = inputNeuronWeight[neuronIndex];
                for (unsigned int inputIndex = 0;
                        inputIndex < lowerSize; ++inputIndex)
                {
                    thisNeuronValue +=
                            inputNeuronValue[inputIndex] *
                            thisWeight[inputIndex];
                }
                thisNeuronValue =
                        modify(thisNeuronValue,
                        thisNeuronType[neuronIndex]);
                outputNeuronValue[neuronIndex+1] = thisNeuronValue;
            }
        }
        // Final layer has no bias value.
        unsigned int lowerSize = upperSize + 1;
        upperSize = layerSize[layers-1];
        double* inputNeuronValue = neuronValue[layers-2];
        double* outputNeuronValue = neuronValue[layers-1];
        double** inputNeuronWeight = weight[layers-2];
        NeuronType* thisNeuronType = neuronType[layers-2];
        for (unsigned int neuronIndex = 0;
                neuronIndex < upperSize; ++neuronIndex)
        {
            double thisNeuronValue = 0;
            double* thisWeight = inputNeuronWeight[neuronIndex];
            for (unsigned int inputIndex = 0;
                    inputIndex < lowerSize; ++inputIndex)
            {
                thisNeuronValue +=
                        inputNeuronValue[inputIndex] *
                        thisWeight[inputIndex];
            }
            thisNeuronValue =
                    modify(thisNeuronValue,
                    thisNeuronType[neuronIndex]);
            outputNeuronValue[neuronIndex] = thisNeuronValue;
        }
        return true;
    }

    bool NeuralNetwork::backPropagateErrors(const Vector& target)
    {
        if (target.size() != layerSize[layers-1]) return false;
        if (layers == 1)
        {
            unsigned int upperSize = layerSize[0];
            for (unsigned int neuronIndex = 0; neuronIndex < upperSize;
                    ++neuronIndex)
            {
                error[0][neuronIndex] = neuronValue[layers-1][neuronIndex] -
                        target[neuronIndex];
            }
            return true;
        }
        // Calculate final error.
        unsigned int upperSize = layerSize[layers-1];
        double* finalError = error[layers-2];
        double* outputNeuronValue = neuronValue[layers-1];
        for (unsigned int neuronIndex = 0; neuronIndex < upperSize;
                ++neuronIndex)
        {
            finalError[neuronIndex] = outputNeuronValue[neuronIndex] -
                    target[neuronIndex];
            // Note: We do not multiply by the derivative of the neuron output,
            // because we are using a modified cost function, but it still has
            // the same derivative.
        }
        // Iteratively calculate errors from top layer to bottom layer.
        unsigned int lowerSize = layerSize[layers-1];
        for (unsigned int layerIndex = layers-2; layerIndex >= 1; --layerIndex)
        {
            upperSize = lowerSize;
            lowerSize = layerSize[layerIndex];
            double* upperError = error[layerIndex];
            double** upperWeight = weight[layerIndex];
            outputNeuronValue = neuronValue[layerIndex];
            NeuronType* thisNeuronType = neuronType[layerIndex-1];
            for (unsigned int inputIndex = 0;
                    inputIndex < lowerSize; ++inputIndex)
            {
                // Error is the weighted sum of the errors from the layer above.
                double thisError = 0;
                for (unsigned int neuronIndex = 0;
                        neuronIndex < upperSize; ++neuronIndex)
                {
                    thisError +=
                            upperWeight[neuronIndex][inputIndex+1] *
                            upperError[neuronIndex];
                }
                // ... multiplied by the derivative of the neuron output.
                thisError *=
                        ma::derivative(outputNeuronValue[inputIndex+1],
                        thisNeuronType[inputIndex], false);
                error[layerIndex-1][inputIndex] = thisError;
            }
        }
        return true;
    }

    bool approxEqual(const NeuralNetwork& left, const NeuralNetwork& right,
            const double error)
    {
        unsigned int leftLayers = left.getLayers();
        unsigned int rightLayers = right.getLayers();
        if (leftLayers != rightLayers) return false;
        Vector leftLayerSize = left.getLayerSizes();
        Vector rightLayerSize = right.getLayerSizes();
        if (leftLayerSize != rightLayerSize) return false;
        Vector leftWeight = left.getWeights();
        Vector rightWeight = right.getWeights();
        if (!approxEqual(leftWeight, rightWeight, error)) return false;
        for (unsigned int layerIndex = 1; layerIndex < leftLayers; ++layerIndex)
        {
            for (unsigned int neuronIndex = 0;
                    neuronIndex < leftLayerSize[layerIndex]; ++neuronIndex)
            {
                if (left.getNeuronType(layerIndex, neuronIndex) !=
                    right.getNeuronType(layerIndex, neuronIndex)) return false;
            }
        }
        return true;
    }

    std::istream& operator>>(std::istream& is, NeuralNetwork& destination)
    {
        Vector layerSize;
        is >> layerSize;
        if (is.good())
        {
            NeuralNetwork result(layerSize);
            bool success = true;
            if (layerSize.size() > 0)
            {
                Vector weight;
                Vector bias;
                is >> weight;
                is >> bias;
                for (unsigned int layerIndex = 1; layerIndex < layerSize.size();
                    ++layerIndex)
                {
                    for (unsigned int neuronIndex = 0;
                            neuronIndex < layerSize[layerIndex]; ++neuronIndex)
                    {
                        unsigned int neuronType;
                        is >> neuronType;
                        success &= result.setNeuronType(layerIndex, neuronIndex,
                                (NeuronType)neuronType);
                    }
                }
                success &= result.setWeights(weight);
                success &= result.setBiases(bias);
            }
            if (success && is.good())
            {
                swap(destination, result);
            }
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const NeuralNetwork& source)
    {
        unsigned int layers = source.getLayers();
        Vector layerSize = source.getLayerSizes();
        os << layerSize;
        if (layers > 0)
        {
            os << '\t' << source.getWeights();
            os << '\t' << source.getBiases();
            for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
            {
                for (unsigned int neuronIndex = 0;
                        neuronIndex < layerSize[layerIndex]; ++neuronIndex)
                {
                    os << '\t' << (unsigned int)source.getNeuronType(
                            layerIndex, neuronIndex);
                }
            }
        }
        return os;
    }

    void print(const NeuralNetwork& source, std::ostream& os)
    {
        source.print(os);
    }
}
