// (c) 2016 - 2017: Marcel Admiraal

#include "tdnetlearner.h"

namespace ma
{
    TDNetLearner::TDNetLearner(const unsigned int stateLength,
            const unsigned int outputLength, const double learnRate,
            const double discountRate, const double gamma,
            const double lambda) :
        NetLearner(stateLength, outputLength, learnRate, discountRate, gamma),
        lambda(lambda), isPreviousState(false), isPreviousGradient(false)
    {
        // Create a default network.
        unsigned int layerArray[] = {stateLength, stateLength*2, outputLength};
        Vector layerSize(layerArray, 3);
        createNetwork(layerSize);
    }

    TDNetLearner::TDNetLearner(const Vector& layerSize,
            const double learnRate, const double discountRate,
            const double gamma, const double lambda) :
        NetLearner(layerSize, learnRate, discountRate, gamma), lambda(lambda),
        isPreviousState(false), isPreviousGradient(false)
    {
    }

    TDNetLearner::TDNetLearner(const NeuralNetwork network,
            const double learnRate, const double discountRate,
            const double gamma, const double lambda) :
        NetLearner(network, learnRate, discountRate, gamma), lambda(lambda),
        isPreviousState(false), isPreviousGradient(false)
    {
    }

    TDNetLearner::TDNetLearner(const char* filename) :
        NetLearner(),
        isPreviousState(false), isPreviousGradient(false)
    {
        load(filename);
    }

    TDNetLearner::TDNetLearner(const TDNetLearner& source) :
        NetLearner(source), lambda(source.lambda),
        isPreviousState(source.isPreviousState),
        isPreviousGradient(source.isPreviousGradient)
    {
    }

    TDNetLearner::~TDNetLearner()
    {
    }

    void swap(TDNetLearner& first, TDNetLearner& second)
    {
        swap(static_cast<NetLearner&>(first), static_cast<NetLearner&>(second));
        std::swap(first.lambda, second.lambda);
        std::swap(first.isPreviousState, second.isPreviousState);
        std::swap(first.isPreviousGradient, second.isPreviousGradient);
        swap(first.previousState, second.previousState);
        swap(first.predictionGradientSum, second.predictionGradientSum);
    }

    TDNetLearner& TDNetLearner::operator=(TDNetLearner source)
    {
        swap(*this, source);
        return *this;
    }

    bool TDNetLearner::save(const char* filename) const
    {
        std::ofstream fileStream(filename, std::ofstream::out);
        if (fileStream.is_open())
        {
            fileStream << *this;
            fileStream << std::endl;
        }
        if (fileStream.good())
        {
            return true;
        }
        return false;
    }

    bool TDNetLearner::load(const char* filename)
    {
        std::ifstream fileStream(filename, std::ifstream::in);
        if (fileStream.good())
        {
            TDNetLearner result;
            fileStream >> result;
            if (fileStream.good())
            {
                swap(*this, result);
                return true;
            }
        }
        return false;
    }

    void TDNetLearner::nextState(const Vector& state, const Vector& reward)
    {
        Vector output = network.getOutput(state);
        output *= discountRate;
        if (reward.size() == output.size()) output += reward;
        updateVariables(output);
        // Save the input.
        previousState = state;
        isPreviousState = true;
    }

    void TDNetLearner::finalReward(const Vector& reward)
    {
        updateVariables(reward);
        sequenceReset();
    }

    void TDNetLearner::sequenceReset()
    {
        isPreviousState = false;
        isPreviousGradient = false;
    }

    double TDNetLearner::getLambda() const
    {
        return lambda;
    }

    void TDNetLearner::setLambda(const double newLambda)
    {
        lambda = newLambda;
    }

    void TDNetLearner::updateVariables(const Vector& target)
    {
        // If there was a previous input.
        if (isPreviousState)
        {
            // Calculate and store the errors.
            Vector error = network.getErrors(target, previousState);
            // Store the inputs.
            Vector neuronInput = network.getNeuronInputs();
            // Update the prediction gradient sum.
            // Discount previous values.
            if (isPreviousGradient)
            {
                predictionGradientSum *= lambda;
                predictionGradientSum += neuronInput;
            }
            else
            {
                // Note: The prediction gradient with respect to the
                // weights, is simply that input, since each input is
                // independent of the others.
                predictionGradientSum = neuronInput;
                isPreviousGradient = true;
            }

            unsigned int parameter = 0;
            unsigned int errorIndex = 0;
            unsigned int sumStartIndex = 0;
            unsigned int layers = network.getLayers();
            Vector layerSize = network.getLayerSizes();
            unsigned int upperSize = layerSize[0];
            Vector weight = network.getWeights();
            Vector weightDelta(weight.size());
            for (unsigned int layerIndex = 1; layerIndex < layers; ++layerIndex)
            {
                unsigned int lowerSize = upperSize;
                upperSize = layerSize[layerIndex];
                // For each layer, the weight changes are the outer product of
                // the prediction gradient sums and the errors multiplied by the
                // alpha.
                for (unsigned int neuronIndex = 0; neuronIndex < upperSize;
                        ++neuronIndex)
                {
                    for (unsigned int inputIndex = 0; inputIndex < lowerSize+1;
                            ++inputIndex)
                    {
                        weightDelta[parameter] =
                                -learnRate * (error[errorIndex] *
                                predictionGradientSum[sumStartIndex+inputIndex]
                                + weight[parameter] * gamma);
                        ++parameter;
                    }
                    ++errorIndex;
                }
                sumStartIndex += lowerSize+1;
            }
            // Update the weights.
            weight += weightDelta;
            network.setWeights(weight);
        }
    }

    std::istream& operator>>(std::istream& is, TDNetLearner& destination)
    {
        double learnRate;
        double discountRate;
        double gamma;
        double lambda;
        NeuralNetwork network;
        is >> learnRate;
        is >> discountRate;
        is >> gamma;
        is >> lambda;
        is >> network;
        if (is.good())
        {
            destination.setLearningRate(learnRate);
            destination.setDiscountRate(discountRate);
            destination.setRegularisationFactor(gamma);
            destination.setLambda(lambda);
            destination.setNetwork(network);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const TDNetLearner& source)
    {
        os << source.getLearningRate() << '\t';
        os << source.getDiscountRate() << '\t';
        os << source.getRegularisationFactor() << '\t';
        os << source.getLambda() << '\t';
        os << source.getNetwork();
        return os;
    }
}
